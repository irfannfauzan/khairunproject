import 'package:aplikasicovid/services/model/model.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;

Future<VaksinModel> getDataVaksin() async {
  final Uri _url = Uri.parse('https://vaksincovid19-api.vercel.app/api/vaksin');
  final response = await http.get(_url);
  if (response.statusCode == 200) {
    final jsonVaksin = jsonDecode(response.body);
    return VaksinModel.fromJson(jsonVaksin);
  } else {
    throw Exception();
  }
}

Future<CovidModel> getDataCovid() async {
  final Uri _url =
      Uri.parse('https://apicovid19indonesia-v2.vercel.app/api/indonesia');
  final responses = await http.get(_url);
  if (responses.statusCode == 200) {
    final jsonCovid = jsonDecode(responses.body);
    return CovidModel.fromJson(jsonCovid);
  } else {
    throw Exception();
  }
}
