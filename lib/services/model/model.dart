class VaksinModel {
  final int sasaranvaksinlansia;
  final int sasaranvaksinpetugaspublik;
  final int vaksinasi1;
  final int vaksinasi2;

  VaksinModel(
      {required this.sasaranvaksinlansia,
      required this.sasaranvaksinpetugaspublik,
      required this.vaksinasi1,
      required this.vaksinasi2});

  factory VaksinModel.fromJson(final json) {
    return VaksinModel(
        sasaranvaksinlansia: json['sasaranvaksinlansia'],
        sasaranvaksinpetugaspublik: json['sasaranvaksinpetugaspublik'],
        vaksinasi1: json['vaksinasi1'],
        vaksinasi2: json['vaksinasi2']);
  }
}

class CovidModel {
  final int positif;
  final int dirawat;
  final int sembuh;
  final int meninggal;

  CovidModel(
      {required this.positif,
      required this.dirawat,
      required this.sembuh,
      required this.meninggal});

  factory CovidModel.fromJson(final json) {
    return CovidModel(
        positif: json['positif'],
        dirawat: json['dirawat'],
        sembuh: json['sembuh'],
        meninggal: json['meninggal']);
  }
}
