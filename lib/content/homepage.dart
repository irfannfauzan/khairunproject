import 'package:aplikasicovid/content/isicontent/datacovid.dart';
import 'package:aplikasicovid/content/isicontent/datavaksin.dart';
import 'package:aplikasicovid/source/source.dart';
import 'package:flutter/material.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(primaryColor: primaryBlack),
      home: Scaffold(
        appBar: AppBar(
          title: Text("Covid-19"),
        ),
        body: SingleChildScrollView(
          scrollDirection: Axis.vertical,
          child: Column(
            children: <Widget>[
              Container(
                alignment: Alignment.center,
                height: 100,
                color: Colors.orange[100],
                padding: EdgeInsets.all(10),
                child: Text(
                  Source.quotes,
                  style: TextStyle(
                      color: Colors.orange[800],
                      fontWeight: FontWeight.bold,
                      fontSize: 16),
                ),
              ),
              SizedBox(
                height: 3,
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Text(
                      "Data Vaksinasi Covid-19",
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 20,
                          color: primaryBlack),
                    ),
                  ],
                ),
              ),
              DataVaksin(),
              SizedBox(
                height: 3,
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Text(
                      "Data Covid-19 Indonesia",
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 20,
                          color: primaryBlack),
                    ),
                  ],
                ),
              ),
              DataCovid(),
              SizedBox(
                height: 3,
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Text(
                      "Edukasi Covid-19",
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 20,
                          color: primaryBlack),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 11.0, right: 11.0),
                child: Container(
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(13),
                      border: Border.all(color: primaryBlack)),
                  child: ListTile(
                    title: Text(
                      "Ciri - Ciri Covid-19",
                      style:
                          TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
                    ),
                    leading: CircleAvatar(
                      backgroundImage: AssetImage('images/corona.jpg'),
                    ),
                    trailing: Icon(Icons.chevron_right),
                  ),
                ),
              ),
              SizedBox(
                height: 15,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 11.0, right: 11.0),
                child: Container(
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(13),
                      border: Border.all(color: primaryBlack)),
                  child: GestureDetector(
                    onTap: () {
                      print("TAP1");
                    },
                    child: ListTile(
                      title: Text(
                        "Cara Mencegah Covid-19",
                        style: TextStyle(
                            fontSize: 15, fontWeight: FontWeight.bold),
                      ),
                      leading: CircleAvatar(
                        backgroundImage: AssetImage('images/mencegah.jpg'),
                      ),
                      trailing: Icon(Icons.chevron_right),
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 10,
              )
            ],
          ),
        ),
      ),
    );
  }
}
