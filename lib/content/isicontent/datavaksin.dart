import 'package:aplikasicovid/services/model/model.dart';
import 'package:aplikasicovid/services/repository/repository.dart';
import 'package:aplikasicovid/source/source.dart';
import 'package:flutter/material.dart';

class DataVaksin extends StatefulWidget {
  const DataVaksin({Key? key}) : super(key: key);

  @override
  _DataVaksinState createState() => _DataVaksinState();
}

class _DataVaksinState extends State<DataVaksin> {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder<VaksinModel>(
      future: getDataVaksin(),
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          final res = snapshot.data;
          return GridView(
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2, childAspectRatio: 1),
            children: <Widget>[
              Views(
                image: 'images/vaksin1.png',
                textColor: primaryBlack,
                title: "Vaksinasi 1",
                count: res!.vaksinasi1.toString(),
              ),
              Views(
                image: 'images/vaksin2.png',
                textColor: primaryBlack,
                title: "Vaksinasi 2",
                count: res.vaksinasi2.toString(),
              ),
              Views(
                image: 'images/lansia.jpg',
                textColor: primaryBlack,
                title: "Lansia",
                count: res.sasaranvaksinlansia.toString(),
              ),
              Views(
                image: 'images/nakes.jpg',
                textColor: primaryBlack,
                title: "Petugas Publik",
                count: res.sasaranvaksinpetugaspublik.toString(),
              ),
            ],
          );
        }
        return CircularProgressIndicator();
      },
    );
  }
}

class Views extends StatelessWidget {
  final String image;
  final Color textColor;
  final String title;
  final String count;

  const Views({
    Key? key,
    required this.image,
    required this.textColor,
    required this.title,
    required this.count,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: EdgeInsets.all(5),
      child: Column(
        children: <Widget>[
          Text(
            title,
            style: TextStyle(
                fontWeight: FontWeight.bold, fontSize: 15, color: textColor),
          ),
          SizedBox(
            height: 3,
          ),
          Image(
            image: AssetImage(image),
            height: 140,
          ),
          Text(
            count,
            style: TextStyle(
                fontWeight: FontWeight.bold, fontSize: 15, color: textColor),
          ),
        ],
      ),
    );
  }
}
