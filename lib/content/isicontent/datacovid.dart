import 'package:aplikasicovid/services/model/model.dart';
import 'package:aplikasicovid/services/repository/repository.dart';
import 'package:aplikasicovid/source/source.dart';
import 'package:flutter/material.dart';

class DataCovid extends StatefulWidget {
  const DataCovid({Key? key}) : super(key: key);

  @override
  _DataCovidState createState() => _DataCovidState();
}

class _DataCovidState extends State<DataCovid> {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder<CovidModel>(
      future: getDataCovid(),
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          final res = snapshot.data;
          return GridView(
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2, childAspectRatio: 1),
            children: <Widget>[
              Views(
                image: 'images/positif.jpg',
                textColor: primaryBlack,
                title: "Positif",
                count: res!.positif.toString(),
              ),
              Views(
                image: 'images/sembuh.jpg',
                textColor: primaryBlack,
                title: "Sembuh",
                count: res.sembuh.toString(),
              ),
              Views(
                image: 'images/dirawat.jpg',
                textColor: primaryBlack,
                title: "Dirawat",
                count: res.dirawat.toString(),
              ),
              Views(
                image: 'images/meninggal.jpg',
                textColor: primaryBlack,
                title: "Meninggal",
                count: res.meninggal.toString(),
              ),
            ],
          );
        }
        return CircularProgressIndicator();
      },
    );
  }
}

class Views extends StatelessWidget {
  final String image;
  final Color textColor;
  final String title;
  final String count;

  const Views({
    Key? key,
    required this.image,
    required this.textColor,
    required this.title,
    required this.count,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: EdgeInsets.all(5),
      child: Column(
        children: <Widget>[
          Text(
            title,
            style: TextStyle(
                fontWeight: FontWeight.bold, fontSize: 15, color: textColor),
          ),
          SizedBox(
            height: 3,
          ),
          Image(
            image: AssetImage(image),
            height: 140,
          ),
          Text(
            count,
            style: TextStyle(
                fontWeight: FontWeight.bold, fontSize: 15, color: textColor),
          ),
        ],
      ),
    );
  }
}
